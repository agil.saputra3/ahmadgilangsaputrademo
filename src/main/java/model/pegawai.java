package model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PEGAWAI")

public class pegawai {

	private long ID;
	private String NAMA_PEGAWAI;
	private String JENIS_KELAMIN;
	public pegawai() {
		super();
	}
	
	public pegawai(long iD, String nAMA_PEGAWAI, String jENIS_KELAMIN) {
		super();
		ID = iD;
		NAMA_PEGAWAI = nAMA_PEGAWAI;
		JENIS_KELAMIN = jENIS_KELAMIN;
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getNAMA_PEGAWAI() {
		return NAMA_PEGAWAI;
	}

	public void setNAMA_PEGAWAI(String nAMA_PEGAWAI) {
		NAMA_PEGAWAI = nAMA_PEGAWAI;
	}

	public String getJENIS_KELAMIN() {
		return JENIS_KELAMIN;
	}

	public void setJENIS_KELAMIN(String jENIS_KELAMIN) {
		JENIS_KELAMIN = jENIS_KELAMIN;
	}

	
}
