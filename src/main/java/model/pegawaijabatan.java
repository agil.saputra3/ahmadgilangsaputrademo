package model;

public class pegawaijabatan {

	private int ID;
	private int ID_PEGAWAI;
	private int ID_JABATAN;
	private String MULAI_KONTRAK;
	private String AKHIR_KONTRAK;
	
	public pegawaijabatan() {
		super();
	}

	public pegawaijabatan(int iD, int iD_PEGAWAI, int iD_JABATAN, String mULAI_KONTRAK, String aKHIR_KONTRAK) {
		super();
		ID = iD;
		ID_PEGAWAI = iD_PEGAWAI;
		ID_JABATAN = iD_JABATAN;
		MULAI_KONTRAK = mULAI_KONTRAK;
		AKHIR_KONTRAK = aKHIR_KONTRAK;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getID_PEGAWAI() {
		return ID_PEGAWAI;
	}

	public void setID_PEGAWAI(int iD_PEGAWAI) {
		ID_PEGAWAI = iD_PEGAWAI;
	}

	public int getID_JABATAN() {
		return ID_JABATAN;
	}

	public void setID_JABATAN(int iD_JABATAN) {
		ID_JABATAN = iD_JABATAN;
	}

	public String getMULAI_KONTRAK() {
		return MULAI_KONTRAK;
	}

	public void setMULAI_KONTRAK(String mULAI_KONTRAK) {
		MULAI_KONTRAK = mULAI_KONTRAK;
	}

	public String getAKHIR_KONTRAK() {
		return AKHIR_KONTRAK;
	}

	public void setAKHIR_KONTRAK(String aKHIR_KONTRAK) {
		AKHIR_KONTRAK = aKHIR_KONTRAK;
	}
	
	
}
