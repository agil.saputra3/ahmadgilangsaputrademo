package model;

public class jabatan {
private int ID_JABATAN;
private String NAMA_JABATAN;

public jabatan() {
	
}

public jabatan(int iD_JABATAN, String nAMA_JABATAN) {
	super();
	ID_JABATAN = iD_JABATAN;
	NAMA_JABATAN = nAMA_JABATAN;
}

public int getID_JABATAN() {
	return ID_JABATAN;
}

public void setID_JABATAN(int iD_JABATAN) {
	ID_JABATAN = iD_JABATAN;
}

public String getNAMA_JABATAN() {
	return NAMA_JABATAN;
}

public void setNAMA_JABATAN(String nAMA_JABATAN) {
	NAMA_JABATAN = nAMA_JABATAN;
}

}
