package controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import exception.ResourceNotFoundException;
import model.pegawai;
import repository.pegawaiRepository;

@RestController
@RequestMapping("/api/v1")
public class pegawaicontroller {

	@Autowired
	private pegawaiRepository pegawaiRepository;
	
	@GetMapping("/getall")
	public List<pegawai> getAllUser() {
		return pegawaiRepository.findAll();
	}
	
	@GetMapping("/getByID/{id}")
	public ResponseEntity<pegawai> getUserById(@PathVariable(value = "id") Long ID)
			throws ResourceNotFoundException {
		pegawai pgw = pegawaiRepository.findById(ID)
				.orElseThrow(() -> new ResourceNotFoundException("User Can not find by this id :: " + ID));
		return ResponseEntity.ok().body(pgw);
	}
	
	@PostMapping("/post")
	public pegawai createPegawai(@RequestBody pegawai pgw) {
		return pegawaiRepository.save(pgw);
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Boolean> deletePegawai(@PathVariable(value = "id") Long ID)
			throws ResourceNotFoundException {
		pegawai pgw = pegawaiRepository.findById(ID)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + ID));

		pegawaiRepository.delete(pgw);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
	
	@PutMapping("/put/{id}")
	public ResponseEntity<pegawai> updatePegawai(@PathVariable(value = "id") Long ID,
			@RequestBody pegawai pegawaiDetails) throws ResourceNotFoundException {
		pegawai pgw = pegawaiRepository.findById(ID)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + ID));
		pgw.setNAMA_PEGAWAI(pegawaiDetails.getNAMA_PEGAWAI());
		pgw.setJENIS_KELAMIN(pegawaiDetails.getJENIS_KELAMIN());
		final pegawai updatePegawai = pegawaiRepository.save(pgw);
		return ResponseEntity.ok(updatePegawai);
	}
}
