package repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.pegawai;

@Repository
public interface pegawaiRepository extends JpaRepository<pegawai, Long>{
pegawai findById(long ID);
	
}
